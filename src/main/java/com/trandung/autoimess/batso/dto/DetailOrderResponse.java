package com.trandung.autoimess.batso.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class DetailOrderResponse {
    public OrderDetailData data;

    @Data
    public static class OrderDetailData {
        public OrderDetailItem item;
    }

    @Data
    public static class OrderDetailItem {
        public String id;
        @JsonProperty(value = "so_dt_sim")
        public String soDtSim;

        @JsonProperty(value = "ma_giu_so")
        public String maGiuSo;

        @JsonProperty(value = "date_create")
        public String dateCreate;

        @JsonProperty(value = "city_name")
        public String cityName;

        @JsonProperty(value = "loai_tb")
        public int loaiThueBao;
    }
}
