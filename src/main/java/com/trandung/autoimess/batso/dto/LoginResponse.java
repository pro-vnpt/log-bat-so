package com.trandung.autoimess.batso.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.io.Serializable;

@Data
public class LoginResponse implements Serializable {
    private boolean success;
    private int statusCode;
    private LoginDataResponse data;


    @Data
    public static class LoginDataResponse implements Serializable {
        public int errorCode;
        private String errorMessage;
        private LoginItemResponse item;
    }

    @Data
    public static class LoginItemResponse {
        private String id;
        private String username;
        @JsonProperty("access_token")
        private String accessToken;
    }
}
