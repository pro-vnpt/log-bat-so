package com.trandung.autoimess.batso.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.List;

@Data
public class AllOrderResponse {
    public boolean success;
    public int statusCode;
    public GetListDataResponse data;

    @Data
    public static class GetListDataResponse {
        List<OrderItem> items;
    }


    @Data
    public static class OrderItem {
        public String id;
        @JsonProperty(value = "customer_name")
        public String customerName;

        @JsonProperty(value = "created_at")
        public String createdAt;


    }
}
