package com.trandung.autoimess.batso.repository;

import com.trandung.autoimess.batso.model.LogBatSo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface LogBatSoRepository extends JpaRepository<LogBatSo, Long> {
    List<LogBatSo> findAllByUsernameAndTypeInAndCodeNull(String username, List<String> types);

    List<LogBatSo> findAllByUsernameAndTypeIn(String username, List<String> types);
    Optional<LogBatSo> findFirstByContentAndUsernameAndTypeIn(String content, String username, List<String> types);
    Optional<LogBatSo> findFirstByContentAndUsernameAndTypeAndNote(String content, String username,String type, String note);
}
