package com.trandung.autoimess.batso.repository;

import com.trandung.autoimess.batso.model.Message;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MessageRepository extends JpaRepository<Message, Long> {
}
