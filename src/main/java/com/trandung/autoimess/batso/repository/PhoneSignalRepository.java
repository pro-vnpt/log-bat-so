package com.trandung.autoimess.batso.repository;

import com.trandung.autoimess.batso.model.PhoneSignal;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PhoneSignalRepository extends JpaRepository<PhoneSignal, Long> {
}
