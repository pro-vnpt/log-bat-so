package com.trandung.autoimess.batso.api;

import com.trandung.autoimess.batso.model.Token;
import com.trandung.autoimess.batso.service.TokenService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/token")
public class TokenController {
    @Autowired
    TokenService tokenService;

    @PostMapping
    public void save(@RequestBody Token token) {
        tokenService.save(token);
    }
}
