package com.trandung.autoimess.batso.api;

import com.trandung.autoimess.batso.model.LogBatSo;
import com.trandung.autoimess.batso.service.LogBatSoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/maxacthuc")
public class LogBatSoController {
    @Autowired
    LogBatSoService logBatSoService;

    @PostMapping
    public void saveLog(@RequestBody LogBatSo logBatSo) {
        logBatSoService.save(logBatSo);
    }

    @GetMapping(value = "/getAll")
    public ResponseEntity<ResponseBodyDTO<LogBatSo>> getAll(@RequestParam(required = false) String search, Pageable pageable) {
        Page<LogBatSo> page = logBatSoService.getAll(pageable, search);
        ResponseBodyDTO result = new ResponseBodyDTO(pageable, page);
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @GetMapping(value = "/syncAllMxt")
    public void getAllMxt(@RequestParam(required = false) String search, Pageable pageable) {
        logBatSoService.getAllMxt();
    }
}
