package com.trandung.autoimess.batso.api;

import com.trandung.autoimess.batso.model.Log;
import com.trandung.autoimess.batso.service.LogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/logs")
public class LogController {

    @Autowired
    LogService logService;

    @PostMapping
    public void saveLog(@RequestBody Log log) {
        logService.save(log);
    }
}
