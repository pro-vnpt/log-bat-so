package com.trandung.autoimess.batso.api;

import com.trandung.autoimess.batso.model.Log;
import com.trandung.autoimess.batso.model.Message;
import com.trandung.autoimess.batso.service.LogService;
import com.trandung.autoimess.batso.service.MessageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/messages")
public class MessageController {
    @Autowired
    MessageService messageService;

    @PostMapping
    public void save(@RequestBody Message message) {
        messageService.save(message);
    }
}
