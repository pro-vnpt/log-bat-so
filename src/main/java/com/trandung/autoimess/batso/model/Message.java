package com.trandung.autoimess.batso.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.Instant;

@Data
@NoArgsConstructor
@Table(name = "message")
@Entity
public class Message {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id; // tự động gen

    @Column(name = "message_from")
    private String messageFrom;

    @Column(name = "message_to")
    private String messageTo;

    @Column(columnDefinition = "text")
    private String data;

    @Column(name = "created_date")
    private Instant createdDate;
}
