package com.trandung.autoimess.batso.model;

import lombok.Builder;
import lombok.Data;

import javax.persistence.*;
import java.time.Instant;

@Data
@Table(name = "token")
@Entity
@Builder
public class Token {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id; // tự động gen

    @Column(name = "username")
    private String username;

    @Column(name = "content", columnDefinition = "text")
    private String content;

    @Column(name = "created_date")
    private Instant createdDate;
}
