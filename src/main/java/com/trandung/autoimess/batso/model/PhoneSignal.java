package com.trandung.autoimess.batso.model;

import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.Instant;

@Data
@Table(name = "phone_signal")
@Entity
@Builder
public class PhoneSignal {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id; // tự động gen

    @Column(name = "phone")
    private String phone;

    @Column(name = "type")
    private String type;

    @Column(name = "created_date")
    private Instant createdDate;
}
