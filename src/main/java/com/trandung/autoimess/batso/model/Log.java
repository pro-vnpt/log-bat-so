package com.trandung.autoimess.batso.model;

import lombok.Data;

import javax.persistence.*;
import java.time.Instant;
@Data
@Table(name = "log")
@Entity
public class Log {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id; // tự động gen
    @Column(name = "created_date")
    private Instant createdDate;

    @Column(columnDefinition = "text")
    private String data;
}
