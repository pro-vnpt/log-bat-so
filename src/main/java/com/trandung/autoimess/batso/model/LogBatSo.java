package com.trandung.autoimess.batso.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.Instant;

@Data
@NoArgsConstructor
@Table(name = "log_bat_so")
@Entity
public class LogBatSo {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id; // tự động gen

    @Column(name = "username")
    private String username;

    @Column(name = "password")
    private String password;

    @Column(name = "type")
    private String type;

    @Column(name = "content", columnDefinition = "text")
    private String content;

    @Column(name = "ma_giu_so")
    private String code;

    @Column(name = "ngay_chon")
    private String ngayChon;
    @Column(name = "created_date")
    private Instant createdDate;

    @Column(name = "dia_chi")
    private String diaChi;
    @Column(name = "sdt_nhan_ma")
    @JsonProperty(value = "sdt_nhan_ma")
    private String sdtNhanMa;

    @Column(name = "loai_thue_bao")
    @JsonProperty(value = "loai_thue_bao")
    private String loaiThueBao;

    @Column(name = "note")
    private String note;
}
