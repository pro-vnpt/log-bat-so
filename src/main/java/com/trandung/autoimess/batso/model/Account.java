package com.trandung.autoimess.batso.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.Instant;

@Data
@NoArgsConstructor
@Table(name = "account")
@Entity
public class Account {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id; // tự động gen

    @Column(name = "username")
    private String username;

    @Column(name = "password")
    private String password;
}
