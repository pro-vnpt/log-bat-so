package com.trandung.autoimess.batso.service;

import com.trandung.autoimess.batso.model.Account;
import com.trandung.autoimess.batso.model.LogBatSo;
import com.trandung.autoimess.batso.repository.AccountRepository;
import com.trandung.autoimess.batso.repository.LogBatSoRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.task.TaskExecutor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.List;

@Service
@Slf4j
public class LogBatSoService {
    @Autowired
    LogBatSoRepository logBatSoRepository;

    @Autowired
    AccountRepository accountRepository;

    @Autowired
    SyncMxtService syncMxtService;




    public Page<LogBatSo> getAll(Pageable pageable, String search) {
        if (StringUtils.containsWhitespace(search)) {

        }
        Page<LogBatSo> page = logBatSoRepository.findAll(pageable);
        return page;
    }

    public void save(LogBatSo logBatSo) {
        if (logBatSoRepository.findFirstByContentAndUsernameAndTypeAndNote(logBatSo.getContent(), logBatSo.getUsername(), logBatSo.getType(), logBatSo.getNote()).isPresent()) {
            return;
        }
        logBatSo.setCreatedDate(Instant.now());
        logBatSoRepository.save(logBatSo);
    }


    public void getAllMxt() {
        List<Account> accounts = accountRepository.findAll();
        for (Account account : accounts) {
            syncMxtService.syncMxt(account);
        }
    }


}
