package com.trandung.autoimess.batso.service;

import com.trandung.autoimess.batso.dto.AllOrderResponse;
import com.trandung.autoimess.batso.dto.AllOrderResponse.OrderItem;
import com.trandung.autoimess.batso.dto.DetailOrderResponse;
import com.trandung.autoimess.batso.dto.LoginResponse;
import com.trandung.autoimess.batso.model.Account;
import com.trandung.autoimess.batso.model.LogBatSo;
import com.trandung.autoimess.batso.repository.LogBatSoRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.text.SimpleDateFormat;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Slf4j
public class SyncMxtService {
    @Autowired
    LogBatSoRepository logBatSoRepository;

    @Autowired
    RestTemplate restTemplate;

    public static final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy").withZone(ZoneOffset.UTC);

    @Async
    public void syncMxt(Account account){
        log.info("Sync Account {}", account.getUsername());
        List<LogBatSo> list = logBatSoRepository.findAllByUsernameAndTypeInAndCodeNull(account.getUsername(), Arrays.asList("PLACE_SUCCESS","SEARCH_AND_PLACE_SUCCESS"));
        try {
            HttpHeaders headers = new HttpHeaders();
            headers.add("x-api-key","b7og0xhp1vbbuz9e9kbly4swwrsjp8qu");
            headers.setContentType(MediaType.APPLICATION_JSON);
            HttpEntity<String> request = new HttpEntity<>("", headers);
            ResponseEntity<LoginResponse> loginResponse = restTemplate.exchange(String.format("https://shop-ctv.vnpt.vn/api/web/index.php/user/login?username=%s&password=%s", account.getUsername(), account.getPassword()),
                            HttpMethod.POST, request, LoginResponse.class);
            String token = loginResponse.getBody().getData().getItem().getAccessToken();


            // Call API get all order
            headers.add("Authorization","Bearer " + token);
            request = new HttpEntity<>("", headers);
            ResponseEntity<AllOrderResponse> ordersResponse = restTemplate.exchange(String.format("https://shop-ctv.vnpt.vn/api/web/index.php/order/get-list?page=1&page_size=200000&status=0"),
                    HttpMethod.POST, request, AllOrderResponse.class);
            for (OrderItem item : ordersResponse.getBody().getData().getItems()) {

                // trung sdt va trung ngay thi call API get detail
                LogBatSo logBatSo = findLogBatSo(item, list);
                if (logBatSo == null) continue;
                // Call API get detail
                ResponseEntity<DetailOrderResponse> orderDetailResponse = restTemplate.exchange(String.format("https://shop-ctv.vnpt.vn/api/web/index.php/sim-so/get-detail?order_id=%s", item.getId()),
                        HttpMethod.POST, request, DetailOrderResponse.class);
                DetailOrderResponse.OrderDetailItem orderDetailItem = orderDetailResponse.getBody().getData().getItem();

                logBatSo.setNgayChon(orderDetailItem.getDateCreate());
                logBatSo.setCode(orderDetailItem.getMaGiuSo());
                logBatSo.setDiaChi(orderDetailItem.getCityName());
                logBatSoRepository.save(logBatSo);
            }
        } catch (Exception e) {
            log.error("{}",e);
        }
    }

    public LogBatSo findLogBatSo(OrderItem orderItem, List<LogBatSo> list) {
        for (LogBatSo logBatSo : list) {
            if (formatter.format(logBatSo.getCreatedDate().plus(7, ChronoUnit.HOURS)).contains(orderItem.getCreatedAt())
                    && orderItem.getCustomerName().contains(logBatSo.getContent().substring(2))) return logBatSo;
        }
        return null;
    }
}
