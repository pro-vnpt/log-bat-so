package com.trandung.autoimess.batso.service;

import com.trandung.autoimess.batso.model.Log;
import com.trandung.autoimess.batso.model.Token;
import com.trandung.autoimess.batso.repository.LogRepository;
import com.trandung.autoimess.batso.repository.TokenRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.Instant;

@Service
public class TokenService {
    @Autowired
    TokenRepository tokenRepository;

    public void save(Token token) {
        token.setCreatedDate(Instant.now());
        tokenRepository.save(token);
    }
}
