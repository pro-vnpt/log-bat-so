package com.trandung.autoimess.batso.service;

import com.trandung.autoimess.batso.model.Log;
import com.trandung.autoimess.batso.repository.LogRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.Instant;

@Service
public class LogService {
    @Autowired
    LogRepository logRepository;

    public void save(Log log) {
        log.setCreatedDate(Instant.now());
        logRepository.save(log);
    }
}
