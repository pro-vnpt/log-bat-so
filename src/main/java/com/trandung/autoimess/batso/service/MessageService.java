package com.trandung.autoimess.batso.service;

import com.trandung.autoimess.batso.model.Message;
import com.trandung.autoimess.batso.repository.MessageRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.Instant;

@Service
public class MessageService {
    @Autowired
    MessageRepository messageRepository;

    public void save(Message message) {
        message.setCreatedDate(Instant.now());
        messageRepository.save(message);
    }
}
